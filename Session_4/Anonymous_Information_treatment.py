"""
Etant donné que votre travail était remarquable, Anonymous vous a à nouveau contacté avec une autre tâches risquée. Ils manquent de main d'oeuvre et aimeraient que traitiez les données que vous leur avez donné.

Ils veulent que vous construisiez une fonction qui transformera votre précédente sortie en quelque chose de plus simple et plus rapide à traiter. C'est à vous de voir comment transformer l'information en un pattern utilisable!

Créez une fonction treatment(data) pour transformer l'information que vous avez précédemment retourné en un pattern :

Chaque suite d'éléments communs devrait être indiqué par la nature de l'élément suivi de '*' et le nombre d’occurrence sans autre éléments entre.

Exemple:

Avec le code '66AeB7' votre précédente fonction sortirait 'number number vowel-up vowel-low consonant-up number'.

Avec cette sortie en entrée votre nouvelle fonction treatment sortirait la string suivante 'number*2 vowel-up*1 vowel-low*1 consonant-up*1 number*1'

N'hésitez pas à utiliser autant de sous-méthodes que vous jugez nécessaires.
"""

def treatment(data):
    data = data.split(" ")
    msg = ""
    data.append('$$$')
    idx = 0
    count = 1
    while data[idx] != '$$$':
        while data[idx] == data[idx+1]:
            count += 1
            idx += 1
     
        msg += data[idx] + "*" + str(count) + ' '
        count = 1
        idx += 1
    return msg[:-4]   #On ne prend pas l'espace à la fin et l'erreur causée par '$$$'
        