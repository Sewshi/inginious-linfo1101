"""
Écrivez une fonction triangle(n) qui pour un nombre entier donné n, retourne la liste de listes imbriquées.
[ [ 0 ],
  [ 0, 1 ],
  [ 0, 1, 2 ],
  ...,
  [ 0, 1, 2, ..., n ] ]
  """

  def triangle(n):
    liste = []                         #Grande liste
    for k in range (n + 1):               
        j = []                         #On crée n sous-liste
        for i in range (k + 1):
            j.append(i)                #On remplit les sous-listes
        liste.append(j)                #On met les sous-listes dans la grande liste
        
    return liste