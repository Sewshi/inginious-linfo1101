"""
La méthode diff_count(lst) retourne le nombre d'éléments différents contenus dans la liste lst.

Par exemple:

Si lst est égal à [3, 5, 3] alors la méthode devrait retourner 2.
Si tous les éléments sont les mêmes, la méthode devrait retourner 1.
Si la liste lst est vide, elle devrait retourner 0.
"""

def diff_counter(lst):
    if len(lst) == 0:
        return 0
    diff_list = []
    for element in lst:
        if element not in diff_list:
            diff_list.append(element)
    return len(diff_list)