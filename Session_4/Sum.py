"""
La méthode sum(list) retourne la somme des éléments contenus dans list.

Exemple: la somme de [1,2,3] est 6

Notez que votre algorithme devrait être capable de calculer la somme même si la liste list contient des éléments malicieux (pas des nombres).
"""

def sum(list):
  sum = 0
  for element in list:
    if type(element) == float or type(element) == int:
      sum += element
  return sum