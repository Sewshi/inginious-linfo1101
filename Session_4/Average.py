"""
La méthode average(list) retourne la moyenne arithmétique des éléments contenu dans list, sauf si list est vide auquel cas, elle devrait retourner None.
"""

def average(list):
    if len(list) == 0:
      return None
    sum = 0
    for element in list:
      sum += element
    moy = sum / len(list)
    return moy