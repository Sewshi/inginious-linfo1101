"""
Les nombres premiers (https://en.wikipedia.org/wiki/Prime_number) sont des nombres avec les caractéristiques suivantes:

ce sont des nombres plus grands que 1
ils n'ont pas des diviseurs autres que 1 et eux-mêmes.
Des nombres premiers sont 2, 3, 5, 7, 11, 13, ...

Pour calculer les nombres premiers on peut utiliser le crible d'Eratosthène : pour chaque nombre successif, on vérifie si on peut le diviser par un des nombres premiers déjà trouvés. Si non, on peut ajouter le nombre à la liste. Ecrivez la fonction primes(max) qui retourne une liste de tous les nombres premiers jusque max (max inclus si max est un nombre premier). Si max est négatif ou zéro, la liste vide doit être retournée.

Pour limiter la complexité de la solution, decomposez votre solution en sous-problèmes comme nécessaire; utilisez des boucles for.
"""

def is_prime(nb):
    for i in range(2, nb):
        if nb % i == 0:
            return False
    return True

def primes(max):
    list = []
    if max <= 0:
        return list
    for nb in range(2, max+1):
        if nb == 2:
            list.append(nb)
        else:
            if is_prime(nb):
                list.append(nb)
    return list
        