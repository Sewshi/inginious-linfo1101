def positions(p,s):
    """
    pre : p est un pattern, c'est-à-dire une chaîne de
        caractères qui peut contenir des lettres, des chiffres et le caractère '?'
        s contient des lettres et des chiffres mais pas le caractère '?'
    post : retourne une liste des occurrences du pattern p à l'intérieur
        de la chaîne de caractères s. Une occurrence est une sous-chaîne de s
        de même longueur que p qui contient les mêmes caractères que p à
        toutes les positions, '?' peut remplacer tous les caractères.
        en ignorant les majuscules et minuscules
    """
    pass
"""
À titre d'exemple, le code ci-dessous :
print(positions("ab","CDEF"))
print(positions("?B","CAbDEF"))
print(positions("A?","CABDEACF"))
print(positions("aa","CAAABDEAAF"))
print(positions("??","CAAAB"))
Affiche les valeurs [], [1], [1,5], [1,2,7] et [0,1,2,3].
"""

def match(pattern, string):
    for i in range(len(string)):
        p = pattern[i].lower()
        if not p == '?' and not p == string[i].lower():
            return False
    return True


def positions(pattern, string):
    result = []
    for i in range(0, len(string) - len(pattern) + 1):
        if match(pattern, string[i:i + len(pattern)]):
            result.append(i)
    return result