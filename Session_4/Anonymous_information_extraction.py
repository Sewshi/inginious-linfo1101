"""
Anonymous vient de vous engager sur le dark web pour une tâche dangereuse. Ils essayent de craquer un code depuis quelques jours mais ne sont arrivés à rien... pour le moment!

Ils veulent que vous construisiez une fonction qui analysera chaque caractère dans un code donné et que vous déterminiez sa nature. Le but est simple : ils ont l'intention d'utiliser les informations que vous leur fournirez pour trouver un pattern dans le code.

Créez une fonction extract(code) pour fournir des infos concernant la nature de chaque élément du code :

Par exemple, si le code 'AeB7' est donné en entrée, la fonction devrait produire 'vowel-up vowel-low consonant-up number' comme sortie. En général :

Ajoutez un number à la réponse si l'élément du code est un chiffre.
Ajoutez un vowel à la réponse si l'élément du code est une voyelle.
Ajoutez un consonant à la réponse si l'élément du code est une consonne.
Suivez cela par -up si la voyelle/consonne est en majuscule.
Suivez cela par -low si la voyelle/consonne est en minuscule.

Exemple :

Avec le code '65AeBc7' la fonction devrait sortir number number vowel-up vowel-low consonant-up consonant-up number
"""

def extract(code):
    ex = ""
    vowel = ("aeiouy", "AEIOUY")
    consonant = ("bcdfghjklmnpqrstvwxz", "BCDFGHJKLMNPQRSTVWXZ")
    number = "0123456789"
    for ch in code:
        if ch in vowel[0]:
            ex += "vowel-low "
        elif ch in vowel[1]:
            ex += "vowel-up "
        elif ch in consonant[0]:
            ex += "consonant-low "
        elif ch in consonant[1]:
            ex += "consonant-up "
        elif ch in number:
            ex += "number "

    return ex
        
            