"""
Écrivez une fonction carre(n) qui pour un nombre entier donné n, retourne la matrice
[ [         0,             1, ...,     n - 1 ],
  [         n,         n + 1, ..., 2 * n - 1 ],
  [     2 * n,     2 * n + 1, ..., 3 * n - 1 ],
  ...,
  [ (n-1) * n, (n-1) * n + 1, ..., n * n - 1 ] ]

Par exemple, pour n=4, la fonction retourne
[ [  0,  1,  2,  3 ],
  [  4,  5,  6,  7 ],
  [  8,  9, 10, 11 ],
  [ 12, 13, 14, 15 ] ]
"""

def carre(n):
    list = []
    for i in range(n):
        list.append([(j+(n*i)) for j in range(n)])
    return list