Session 1 :
Opérations mathématiques de base (addition, soustration, ...)
Utilisation du mode math dans Bathutub_with_a_hole
While loops & if/else statements


Session 2 :
For loops
Types (boolean, int, float, ...)


Session 3 :
Math module
Idem que session 2 mais un peu plus compliqué
Fonctions


Session 4 : 
''.format()
For loops intéressantes
Listes
Les exos Anonymous sont intéressants


Session 5 :
Binary Search
Opérations sur les listes (fusion, tri manuel, ...)
Exo Sorting Hat intéressant

