"""
Supposez que les variables a, b et x contiennent un nombre naturel. Ecrivez un fragment de code qui assignerait la valeur booléenne True à une variable nommée interval si x appartient à [a, b]. Assignez la valeur False sinon.
a = ... #limite inférieure de l'intervalle
b = ... #limite supérieure de l'intervalle
x = ... #variable à évaluer
interval = ... #enregistrez dans cette variable l'appartenance de x à l'intervalle
"""


return x >= a and x <= b