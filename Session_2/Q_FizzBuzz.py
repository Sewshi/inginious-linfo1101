"""
Ecrivez un programme qui permet de jouer au jeu fizzbuzz. Vous recevez un nombre (stocké dans la variable i). Nous allons implémenter une version simplifiée du jeu. Pour un entier i donné, le jeu va :

enregistre la string "fizz" dans la variable temp si le nombre est divisible par 3.
enregistre la string "buzz" dans la variable temp si le nombre est divisible par 5.
enregistre la string "fizzbuzz" dans la variable temp si le nombre est divisible par 3 et par 5.
enregistre la string "no" dans la variable temp si le nombre n'est divisible ni par 3, ni par 5.

i = ...    # le nombre à tester (i >= 1)
temp = ... # "fizz", "buzz", "fizzbuzz" ou "no" en fonction de la valeur de i

"""

if i % 3 == 0:
    temp = "fizz"
if i % 5 == 0:
    temp = "buzz"
if i % 3 == 0 and i % 5 == 0:
    temp = "fizzbuzz"
if i % 3 != 0 and i % 5 != 0:
    temp = "no"