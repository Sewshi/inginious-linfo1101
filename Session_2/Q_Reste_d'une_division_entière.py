"""
Supposez que les variables a et b contiennent un nombre naturel.

Ecrivez un fragment de code qui assigne le reste de leur division à la variable rest.

Pour implémenter votre solution, utilisez uniquement une boucle while et des soustractions (la solution la plus simple rest = a % b n'est pas acceptée; nous voulons tester si vous êtes capable d'implémenter une telle opération par vous-même).

Notez que vous ne devez pas permettre la division par 0 et vous devez assigner la la valeur None à rest dans un tel cas.

a = ... #variable à évaluer
b = ... #variable à évaluer
rest = ... #enregistrez dans cette variable le reste de la division a/b
"""

if b == 0 :
    rest = None
else:
    while a - b >= 0:
        a -= b
    rest = a