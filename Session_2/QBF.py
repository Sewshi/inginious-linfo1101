"""
Ecrivez un programme qui affiche à l’écran le nombre de diviseurs propres de chaque entier entre 1 et n, où n est donné dans une variable n. Pour ce faire, il faut travailler en deux étapes:

Tout d’abord, il faut pouvoir calculer le nombre de diviseurs propres de l’entier e (c’est-à-dire le nombre de diviseurs entiers différents de e).
Il suffit ensuite de faire ce test pour tous les entiers i compris entre 1 et n (inclus) et afficher le nombre de diviseurs calculé pour chaque entier.
Pour rappel, vous pouvez utiliser print(i) pour afficher à l’écran la valeur de la variable i.

Par exemple: 2 a un diviseur propre, 3 et 5 également, 4 en a deux, 6 en a trois, 7 en a un, . . .

L'output attendu sera donc pour n = 6 :

0

1

1

2

1

3
"""

for i in range (1, n+1):  
    if i == 1:
        print("1 : 0")
    else:
        count = 0
        for j in range (1, i):
            if i % j == 0:
                count += 1
        print("{} : {}".format(i, count))
        