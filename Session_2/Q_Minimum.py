"""
Supposez que les variables a, b et c contiennent un nombre naturel.

Ecrire un fragment de code qui assigne à la variable min le plus petit de ces nombres.
Ne pas utiliser min()

a = ... #variable à évaluer
b = ... #variable à évaluer
c = ... #variable à évaluer
minima = ... #enregistrez dans cette variable la valeur la plus petite des trois
"""

if a < b and a < c:
    minima = a
if a < b and a > c:
    minima = c
if b < a and b < c:
    minima = b
if b < a and b > c:
    minima = c
if c < a and c < b:
    minima = c
if c < a and c > b:
    minima = b