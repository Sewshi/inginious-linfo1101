"""
Un nombre premier , utilisé par les mathématiciens, est un set de nombres partageant une même propriété.

Un nombre premier est un nombre naturel plus grand que 1 et qui n'a pas d'autres diviseurs positifis à part 1 et lui-même.

Stockez dans is_prime True si le n est un nombre premier et False sinon. is_prime est initialisé à True par défaut.

is_prime = True
n = ...             #Un entier supérieur à 1
"""

is_prime = True
for i in range(2, n//2):
    if n % i == 0:
        is_prime = False