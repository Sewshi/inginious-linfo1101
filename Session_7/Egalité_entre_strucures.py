"""
Données sont:

    une matrice representée en utilisant des listes imbriquées
    une matrice representée en utilisant un dictionnaire, où les zéros ne sont pas stockées.

Par exemple:

l = [[0, 2, 4], [4, 1, 0]]
d = {(0,1): 2, (0,2): 4, (1,0): 4, (1,1): 1}

Écrivez une fonction equal(l,d) qui détermine si d contient les même valeurs que l pour chaque entrée de l. (Nous permettons que d soit plus large que l.)
"""

def equal(l, d):
    #Calculer le nombre de valeurs différentes de 0 dans la liste#
    list_len = 0
    for list in l:
        for element in list:
            if element != 0:
                list_len += 1
    for keys in d.keys():
        x, y = keys
        try:
            if l[x][y] != d[keys]:
                return False
        except:
                pass
    if list_len > len(d.values()):
        return False
    return True