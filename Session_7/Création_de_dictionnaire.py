"""
Une liste de coordonnées est donnée. Par exemple:

l = [(2.0, 5.0), (8.0, 12.0), (10.0, 40.0), (8.0, 50.0), (8.0, 50.0)]

Nous voulons créer un dictionnaire pour identifier rapidement les valeurs y pour un x donné. Par exemple:

d = {2.0: [5.0], 8.0: [12.0, 50.0, 50.0], 10.0: [40.0]}

Écrivez une fonction create_dict(l) qui crée ce dictionnaire (pas limité à cet exemple).
"""

def create_dict(l):
    d = {}
    for tuple in l:
        if tuple[0] not in d:
            d[tuple[0]] = [tuple[1]]
        else:
            d[tuple[0]].append(tuple[1])
    return d