"""
Soit une structure de données comparable à la suivante:

l = [{"City": "Bruxelles", "Country": "Belgium"}, \
    {"City": "Berlin", "Country": "Germany"}, \
    {"City": "Paris", "Country": "France"}]

Écrivez une fonction get_country(l,name) qui, pour le nom d'une ville name et une structure de données l, retourne le nom du pays dans lequel la ville est localisée. La fonction retourne False si la ville n'est pas dans l.
"""

def get_country(l, name):
    for dictionary in l:
        if dictionary["City"] == name:
            return dictionary["Country"]
    return False