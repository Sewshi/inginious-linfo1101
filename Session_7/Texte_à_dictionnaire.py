"""
Écrivez un outil qui remplit les requêtes suivante:

    l'outil lit un fichier text.txt, sépare chaque ligne en mots, et crée un dictionnaire avec, pour chaque mot, un compte du nombre d'occurrences de ce mot. On peut présumer que tous les mots sont en minuscules et qu'il n'y a pas de ponctuation.
    l'outil reçoit un mot en argument, le programme retourne le nombre d'occurrences de ce mot.

Séparez le programme en fonctions, avec au moins

    une fonction create_dictionary(filename) pour lire le fichier filename et créer le dictionnaire
    une autre fonction occ(dictionary, word) pour renvoyer le nombre d'occurences du mot, et 0 si le mot n'est pas dans le dictionnaire.

Pour simplifier l'exercice, il n'est pas nécessaire de traiter les erreurs.
"""

def create_dictionary(filename):
    f = open(filename, "r")
    dictionary = {}
    for line in f:
        line = line.strip().split(" ")
        for word in line:
            dictionary[word] = dictionary.get(word, 0) + 1
    return dictionary

def occ(dictionary, word):
    keys = dictionary.keys()
    for key in keys:
        if key == word:
            return dictionary[key]
    return 0