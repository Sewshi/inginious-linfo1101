"""


Lors de la dernière session du club de duel, les sabliers comptant les points de chaque maison ont été détruits même un reparo n'a rien pu faire et la célébration de la Coupe des Quatre Maisons arrive à grands pas!

Heureusement, Rusard, qui ne fait pas confiance à la magie, a gardé les comptes de tous les accomplissements perpétrés par les étudiants. Il vous a fourni un dictionnaire associant chaque élève à sa maison et un parchemin avec tous leurs gains.

La liste des scores est donnée dans un fichier. Le contenu de ce fichier suit le format suivant :

    Lignes: student_name points

Merci de retourner le nom de la maison gagnante, dans le cas d'un ex-aequo : retournez une liste des meilleures maisons.

S'il y a une erreur, levez une exception.
"""
#Dictionnaire donné dans l'énoncé :
students = {'gryffindor': ['Harry', 'Hermione', 'Ron', 'Ginny', 'Fred', ' Georges', 'Neville'],
            'ravenclaw': ['Cho', 'Luna', 'Sybill', 'Marcus', 'Marietta', 'Terry', 'Penelope'],
            'hufflepuff': ['Pomona', 'Zacharias', 'Teddy', 'Cedric', 'Nymphadora', 'Newton', 'Justin'],
            'slytherin': ['Malfoy', 'Severus', 'Dolores', 'Horace', 'Blaise', 'Pansy', 'Bellatrix']}

def winning_house(scroll):
    wh = {"gryffindor" : 0, "ravenclaw" : 0, "hufflepuff" : 0, "slytherin" : 0}
    f = open(scroll, "r")
    for line in f:
        line = line.strip().split(" ")
        name = line[0]
        points = int(line[1])
        for key in students:
            if name in students[key]:
                wh[key] += points
    most_point = 0
    several_winner = False
    for house in wh:
        if wh[house] > most_point:
            most_point = wh[house]
            best_house = house
        if wh[house] == most_point and house != best_house:
            best_houses = [best_house, house]
            several_winner = True
    if several_winner:
        return best_houses
    else:
        return best_house
        