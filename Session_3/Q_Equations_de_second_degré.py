"""
VOIR 'CONSIGNE_SECOND_DEGRE'

Voici un exemple d'utilisation des fonctions que vous devez implémenter, avec l'équation x2+2x+1=0 :

rho(1, 2, 1)
>>> 0

n_solutions(1, 2, 1)
>>> 1

solution(1, 2, 1)
>>> -1.0
"""

def rho(a, b, c):
    return b**2 - 4 * a * c

def n_solutions(a, b, c):
    if rho(a, b, c) > 0:
        return 2
    if rho(a, b, c) == 0:
        return 1
    if rho(a, b, c) < 0:
        return 0

def solution(a, b, c):
    if rho(a, b, c) > 0:
        return min((0 - b + racine_carree(rho(a, b, c))) / (2 * a), (0 - b - racine_carree(rho(a, b, c))) / (2 * a))
    if rho(a, b, c) == 0:
        return (0 - b) / (2 * a)