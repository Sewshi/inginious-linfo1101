"""
Ecrivez l'en-tête et le corps d'une fonction baptisée afficheMax qui prend deux nombres entiers comme arguments et affiche le plus grand de ces nombres. La spécification de cette fonction est :
pre: --
post: affiche le maximum entre les nombres entiers `a` et `b`
"""

def afficheMax(a, b):
    if a > b:
        print(a)
    else:
        print(b)