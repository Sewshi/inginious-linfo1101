"""
Le Plus Grand Diviseur d'un nombre a est le plus grand nombre (excepté a lui-même) tel que la division de a parce ce nombre naturel est une division entière.

Etant donné que 0 est divisible par n'importe quel nombre naturel, cela risque de poser problème pour retourner le plus grand, nous attendons donc que vous retourniez None dans ce cas. 1 devrait également renvoyer None.

Rappelez-vous que l'opérateur % retourne le reste de la division euclidienne.

Définissez la fonction greatest_divisor(a) de façon àce qu'elle retourne le PGDC d'un nombre a.
"""

def greatest_divisor(a):
    greatest_divisor = None
    for i in range(2, a):
        if a % i == 0:
            greatest_divisor = i
    return greatest_divisor