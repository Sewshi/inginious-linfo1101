"""
L'indice de masse corporelle (BMI) ou indice de Quételet, utilisé par les diététiciens, est révélateur de la condition d'une personne.

Un personne normale devrait avoir un indice se trouvant entre 20 et 25. En dessous de 20, elle est considérée mince et au dessus de 25, elle est en surpoids. Au delà de 30, la personne est obèse!

Cet indice est calculé comme le ratio entre le poids de la personne, exprimée en kg, et le carré de la taille de la personne, exprimée en mètres.

Créez une fonction quetelet(height, weight) qui calcule l'indice de Quételet d'une personne et retourne thin pour les personnes avec un indice strictement plus petits que 20, normal pour une personne dont l'indice est entre 20 et 25, overweight si l'index est strictement plus grand que 25 et moins que ou égal à 30 et obese quand l'indice est plus grand que 30.

Implémentez la fonction quetelet(height, weight) en Python.
"""

def quetelet(height, weight):
    ind = weight/(height**2)
    if ind < 20:
        status = "thin"
    if ind >= 20 and ind <= 25:
        status = "normal"
    if ind > 25 and ind <= 30:
        status = "overweight"
    if ind > 30:
        status = "obese"
    return status