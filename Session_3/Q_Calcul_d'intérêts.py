"""
Supposez que vous avez base € en ce moment sur votre compte épargne.

Si vous le laissez x années sur ce compte avec un taux d'intérêt cumulés de y %, quel sera le solde de votre compte après ces x années?

Retournez ce montant.
"""

def interest(base, y, x):
    return base * ((y+100)/100)**x