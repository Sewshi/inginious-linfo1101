"""
Le module math définit les constantes et fonctions mathématiques usuelles, telles que sqrt(x) (racine carrée), sin(x), cos(x), log(x) (logarithme), pi, etc. Ecrivez un programme qui imprime les valeurs de sin π/n pour n allant de 1 à 10.
"""

import math
for n in range (1, 11):
    print(math.sin(math.pi/n))
