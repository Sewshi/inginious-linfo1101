"""
En mathématiques, la suite de Fibonacci sont les nombres dans la séquence suivante d'entiers, qui est caractérisée par le fait que chaque nombre après les deux premiers est égal à la somme des deux précédents :

/Exemple: 0,1,1,2,3,5,8,13,21,34,55,89,144,...

Un algorithme très simple serait :
F_0 = 0
F_1 = 1 
F_n = F_(n−1) + F_(n−2)
Créez une fonction fibonacci(n) qui retourne le Nième élément de la suite de Fibonacci.
"""

def fibonacci(n):
    c = 0
    a, b = 0, 1
    
    if n == 0:
        return c
    if n == 1:
        return c+1
    
    for i in range (n-1):
        c = a + b
        a = b
        b = c
    return c
    