"""
La police vous engage pour développer un programme qui calcule les amendes qu'un conducteur doit payer s'il a fait une infraction. La loi stipule qui le conducteur doit payer 5 € par km/h au dessus de la limite de vitesse autorisée, avec une amende minimum de 12.5 €.

Pour tous les excès de vitesse de plus de 10 km/h au dessus de la limite, l'amende est DOUBLEE !

Votre programme prend en entrée la vitesse maximale autorisée et la vitesse actuelle de la voiture et calcule l'amende possible.

Exemple:

Si la vitesse maximale est de 50km/h et que le véhicule circule à du 62km/h, la peine sera de 12 * 5 * 2 = 120 €.
Si la vitesse maximale est de 50km/h et que le véhicule circule à du 56km/h, la peine sera de 6 * 5 = 30 €.
Si la vitesse maximale est de 50km/h et que le véhicule circule à du 51km/h, la peine sera de 12.5 €.

Implémentez la fonction fine(authorized_speed, actual_speed) en Python.
"""


def fine(authorized_speed, actual_speed):
    diff = actual_speed - authorized_speed
    fine = 5 * diff
    if diff > 10:
        fine *= 2
    if fine < 12.5:
        fine = 12.5
    if diff < 0:
        fine = 0
    return fine
    