"""
Le Plus Grand Commun Diviseur (Greatest Common Divisor en anglais) de deux nombres naturels a et b est le plus grand nombre naturel k tel que la division de a et b par ce naturel k est une division entière.

Euclide a trouvé un algorithme récursif très simple pour trouver le PGCD de deux nombres :
gcd(a,0) = a
gcd(a,b) = gcd(b,a%b)
Rappelez-vous que l'opérateur % retourne le reste de la division euclidienne.

Définissez une fonction qui retourne le PGDC de deux nombres a et b.
"""

def gcd(a, b):
    while b != 0:
        a, b = b, a % b
    return a