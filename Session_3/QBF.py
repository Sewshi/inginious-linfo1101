"""
Un programme Python a été écrit par un étudiant pour compter le nombre de chiffres de la représentation décimale d'un nombre entier positif. Le programme proposé par l'étudiant est le suivant:

n = 0
while True:
    n = int(input("Entrez un nombre (-1 pour arrêter) : "))
    if n < 0:
        break
    if chiffres_pairs(n):
        print(n, " a un nombre pair de chiffres dans sa représentation décimale")
    else:
        print(n, " a un nombre impair de chiffres dans sa représentation décimale")


Lors de son exécution, le programme affiche la sortie ci-dessous :

Entrez un nombre (-1 pour arrêter) : 7
7 a un nombre impair de chiffres dans sa représentation décimale
Entrez un nombre (-1 pour arrêter) : 1234
1234 a un nombre pair de chiffres dans sa représentation décimale
Entrez un nombre (-1 pour arrêter) : 7888
7888 a un nombre pair de chiffres dans sa représentation décimale
Entrez un nombre (-1 pour arrêter) : -1

Pouvez-vous compléter ce programme en écrivant :

l'en-tête
la spécification
le corps
de la fonction chiffres_pairs utilisée par l'étudiant. Pour cela, utilisez une découpe en sous-problèmes et fournissez les fonctions nécessaires.
"""

def chiffres_pairs(n):
    """
    Retourne True si le nombre a un nombre pair de chiffres dans sa représentation décimale, False sinon.
    """
    return len(str(n)) % 2 == 0