"""
Ecrivez l'en-tête, la spécification et le corps d'une fonction fact(n) qui calcule la factorielle de n : n!=n.(n−1).(n−2)…1. Veillez à ce que la méthode que vous écrivez respecte bien les spécifications que vous définissez vous-même.
"""

def fact(n):
    f = 1
    while n != 1 and n > 0:
        f *= n * (n - 1)
        n -= 2
    return f
