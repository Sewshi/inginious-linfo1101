"""
En sciences informatiques, un algorithme de tri est un algorithme qui place les éléments d'une liste dans un certain ordre. Les ordres les plus utilisés sont l'ordre numérique et l'ordre lexicographique. Un tri efficace est important pour optimiser l'utilisation d'autres algorithmes (tels que les algorithmes de recherche ou de fusion) qui ont besoin d'une liste triée en entrée ; c'est aussi souvent utile pour la canonicalisation des données et pour produire des sorties lisibles par les humains. Plus formellement, la sortie doit satisfaire deux conditions :

- La sortie est en ordre croissant (chaque élément n'est pas plus petit que l'élément précédent selon l'ordre choisi);
- La sortie est une permutation (réorganisation mais avec tous les éléments originaux) de l'entrée.

a_list = ... #liste à trier
sorted_list = ... #enregistrez dans cette variable la version triée de la liste
(Ne pas utiliser sort() ou sorted())
"""

sorted_list = a_list
for i in range (len(sorted_list)):
    for j in range (i + 1, len(sorted_list)):
        if sorted_list[i] > sorted_list[j]:
            sorted_list[i], sorted_list[j] = sorted_list[j], sorted_list[i]