"""
Une fois de plus, c'est l'heure de la cérémonie de la répartition . Les premières années attendent en rangs devant un vieux chapeau, à la fois anxieux et excités. Le directeur fait un long discours pour les accueillir et laisse la place à un mystérieux intervenant.

Tous les premières années sont ébahis lorsque le Choixpeau Magique brise le silence en chantant l'une de ses célèbres chansons.

Cependant, le choixpeau en fait un peu trop et rencontre quelques problèmes. Il perd sa voix et ne sera pas en état d'assurer la suite de la cérémonie. Heureusement, nous avons toujours accès aux connaissances des fondateurs et nous pourrons répartir les élèves avec votre aide.

Créez une fonction house_designation(student_qualities) qui va retourner une liste avec les noms des quatres maisons, la première étant celle où l'étudiant devrait aller et la dernière, celle qui convient le moins à l'étudiant. Pour décider de cette répartition, l'étudiant devrait être placé dans la maison où il a le plus d'affinités, c'est-à-dire, la maison avec laquelle il partage le plus de qualités. Si deux maisons sont à égalité, on les retourne dans l'ordre dans lequel elles sont placées dans les connaissances des fondateurs.
"""

def house_designation(student_qualities):
    designation = [['Gryffindor', 0], ['Ravenclaw', 0], ['Hufflepuff', 0], ['Slytherin', 0]]   #On peut aussi utiliser un dictionnaire, mais c'est plus embêtant pour trier
    for quality in student_qualities:
        if quality in knowledge[0][1]:
            designation[0][1] += 1
            
        elif quality in knowledge[1][1]:
            designation[1][1] += 1
            
        elif quality in knowledge[2][1]:
            designation[2][1] += 1
            
        elif quality in knowledge[3][1]:
            designation[3][1] += 1
            
    designation.sort(key=lambda x:x[1], reverse=True)
    final_result = [i[0] for i in designation]
    return final_result
        