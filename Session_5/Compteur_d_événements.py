"""
Une liste d'événements qui ont lieu à certaines coordonnées est donnée. Un exemple d'une telle liste est la suivante:
events = [(0,1),(2,3),(0,1),(4,5),(1,2),(0,1),(1,1),(0,2),(1,1)]
En paramètre une coordonnée (i,j) est donnée, créez une fonction count(events,i,j) pour compter le nombre d'événement sur cette coordonnée et renvoyez la.
"""

def count(events, i, j):
	count = 0
	for e in events:
		if e == (i, j):
			count += 1
	return count