"""
Supposez que, pour la liste suivante on veut trouver les étudiants d'un cours:

[('LINFO1101', ['Jean', 'Pierre']),('LINFO1110', ['Jean']), ('LINFO1111', ['Jean']), ('LINFO1112', ['Jacques', 'Pierre']), ('LINFO1113', ['Pierre'])]
Donnez un algorithme binary_search(course,list_of_courses) pour trouver tous les étudiants d'un cours dans cette structure de données. Si le cours n'a pas d'étudiant ou n'existe pas, renvoyez une liste vide. Copiez le code du cours de la recherche binaire et modifiez-le.
"""

def binary_search ( name, list_of_names ):
    first = 0
    last = len(list_of_names)-1
    found = False

    while first<=last and not found:
        middle = (first + last)//2
        if list_of_names[middle] == name:
            found = True
        else:
            if name < list_of_names[middle]:
                last = middle-1
            else:
                first = middle+1

    return found