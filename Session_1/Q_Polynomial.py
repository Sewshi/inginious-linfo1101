"""
Vous voulez utiliser votre ordinateur pour facilement calculer un polynôme de la forme suivante : y=ax^6+bx^2+c (a fois x exposant 6 + b fois x exposant 2 + c). Nous vous donnons les coefficients (enregistrer dans a, b et c) et la variable x et vous demandons de donner la valeur du polynôme en abscisse x dans une variable nommée y.

Note: il est possible de faire ceci en une seule ligne de code.
"""

y = a * x**6 + b * x**2 + c