"""
Lucie veut prendre un bain, mais sa baignoire est trouée. Si elle ouvre le robinet, 12L coulent dans la baignoire chaque minute, et 1L s'écoule par le trou chaque minute.

Vous aimeriez savoir combien de minutes elle devra attendre pour que la baignoire contienne au moins 80L d'eau, et aussi simuler la croissance du volume de l'eau en Python.

D'abord, calculez le nombre de minutes nécessaires pour avoir exactement 80L dans la baignoire (exprimé en nombre à virgule flottante) et enregistrez-le dans une variable nommée filled_time. Ensuite, enregistrez le volume d'eau actuel dans une variable nommée water_vol (qui commence à 0) et utilisez une boucle for dans laquelle, à chaque itération (représentant chaque minute), les volumes d'eau s'écoulant du robinet ou par le trou sont ajoutés/retirés du volume d'eau total. En d'autres termes, à chaque itération, water_vol devrait augmenter de 12L et diminuer d'1L.

La boucle for devrait s'arrêter à la première itération où le volume d'eau dépasse les 80L. Ceci peut être calculé à partir de filled_time.

Note: vous pouvez arrondir un nombre à virgule flottante au premier integer plus grand en utilisant le code suivant :
import math
float_nb = 3.14
int_nb = math.ceil(float_nb)  # `int_nb` est égal à 4   
"""

import math
filled_time = 80/11
water_vol = 0
for k in range(math.ceil(filled_time)):
    water_vol+=11