"""
Supposez que les variables a, b et c contiennent un nombre naturel.

Écrivez un fragment de code qui assigne à la variable median la valeur médiane de ces nombres.

Pour rappel, la médiane de trois valeurs est la valeur telle qu'il y a exactement une valeur inférieure (ou égale) et une valeur supérieure (ou égale) à cette valeur.

a = ... #variable à évaluer
b = ... #variable à évaluer
c = ... #variable à évaluer
median = ... #enregistrez dans cette variable la médiane des trois variables
"""


if a < b:
    if b < c:
        median = b
    if b > c:
        if a < c:
            median = c
        if a > c:
            median = a
        if a ==c:
            median = a
    if b == c:
        median = b
if a > b:
    if b > c:
        median =  b
    if b < c:
        if a > c:
            median = c
        if a < c:
            median = a
        if a == c:
            median = a
    if b == c:
        median = b
if a == b:
    median = a