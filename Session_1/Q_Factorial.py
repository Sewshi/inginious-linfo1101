"""
En mathématiques, la factorielle d'un entier strictement positif x, noté x!. La factorielle de 0 est 1. Calculez la factorielle d'un entier x et enregistrez la valeur calculée dans la variable result.

Exemples:

5!=5×4×3×2×1=120
0!=1
"""

result = 1
for i in range (x+1):
    if i != 0:
        result *= i